# angular-fabric

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.15.1.

## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.

## Research

### POS Creation (A4)
https://jsfiddle.net/dr8ozen9/9/

### Loading POS (JSON, editable)
https://jsfiddle.net/gk3qhm29/3/

### angular-fabric demo page
http://codepen.io/anon/pen/jrBWPy

### Basic snapping (to borders & other objects) and collision detection
https://jsfiddle.net/aphillips8/31qbr0vn/1/

### Grid-based snapping (closer to how `Print layout` works)
http://jsfiddle.net/9tB8n/96/

### Example `Print Layout` w/ shelf edge POS (loaded via JSON, as a group) and hard-coded scaling to A4 ratio
https://jsfiddle.net/mL8zf7zc/2/

### Responsive canvas size & scaling elements links
* http://stackoverflow.com/questions/21931271/how-to-enable-responsive-design-for-fabric-js
* http://stackoverflow.com/questions/23630816/make-canvas-element-and-all-its-children-responsive-for-all-screen-sizes
* http://stackoverflow.com/questions/30862356/fabric-js-resize-canvas-to-fit-screen

### Issues w/ loading POS' into a Fabric canvas via SVG
https://github.com/kangax/fabric.js/issues/2302

