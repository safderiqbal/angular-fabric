(function () {
  'use strict';

  function PrintLayoutCtrl() {
    var vm = this;

    vm.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  }

  /**
   * @ngdoc function
   * @name posApp.controller:PrintLayoutCtrl
   * @description
   * # PrintLayoutCtrl
   * Controller of the posApp
   */
  angular.module('posApp')
    .controller('PrintLayoutCtrl', PrintLayoutCtrl);
})();
