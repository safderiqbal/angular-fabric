(function () {
  'use strict';

  function GeneratePosCtrl($scope, $log, Fabric, FabricConstants) {
    var vm = this;

    var methods = {
      init: function () {
        vm.fabric = new Fabric({
          JSONExportProperties: FabricConstants.JSONExportProperties,
          textDefaults: FabricConstants.textDefaults,
          shapeDefaults: FabricConstants.shapeDefaults,
          json: {}
        });
      }
    };

    vm.fabric = {};
    vm.FabricConstants = FabricConstants;
    vm.logFabric = function () {
      $log.info('test');
      $log.debug(vm.fabric);
    }

    vm.selectCanvas = function() {
      vm.canvasCopy = {
        width: vm.fabric.canvasOriginalWidth,
        height: vm.fabric.canvasOriginalHeight
      };
    };

    vm.setCanvasSize = function() {
      vm.fabric.setCanvasSize(vm.canvasCopy.width, vm.canvasCopy.height);
      vm.fabric.setDirty(true);
      delete vm.canvasCopy;
    };

    $scope.$on('canvas:created', methods.init);
  }

  /**
   * @ngdoc function
   * @name posApp.controller:GeneratePosCtrl
   * @description
   * # GeneratePosCtrl
   * Controller of the posApp
   */
  angular.module('posApp')
    .controller('GeneratePosCtrl', GeneratePosCtrl);

  GeneratePosCtrl.$inject = ['$scope', '$log', 	'Fabric', 'FabricConstants'];
})();
