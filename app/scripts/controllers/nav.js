(function () {
  'use strict';

  function NavCtrl($location) {
    var vm = this;

    var methods = {
      isActive: function (tab) {
        return $location.path() === tab;
      }
    };

    vm.isActive = methods.isActive;
  }

  angular
    .module('posApp')
    .controller('NavCtrl', NavCtrl);
})();
