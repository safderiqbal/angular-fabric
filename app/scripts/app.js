(function () {
  'use strict';

  function RouteProviderCnfg($routeProvider) {
    $routeProvider
      .when('/generate-pos', {
        templateUrl: 'views/generate-pos.html',
        controller: 'GeneratePosCtrl',
        controllerAs: 'vm'
      })
      .when('/print-layout', {
        templateUrl: 'views/print-layout.html',
        controller: 'PrintLayoutCtrl',
        controllerAs: 'vm'
      })
      .otherwise({
        redirectTo: '/generate-pos'
      });
  }

  /**
   * @ngdoc overview
   * @name posApp
   * @description
   * # posApp
   *
   * Main module of the application.
   */
  angular
    .module('posApp', [
      'ngAnimate',
      'ngAria',
      'ngCookies',
      'ngMessages',
      'ngResource',
      'ngRoute',
      'ngSanitize',
      'common.fabric',
      'common.fabric.utilities',
      'common.fabric.constants'
    ])
    .config(RouteProviderCnfg);
})();
